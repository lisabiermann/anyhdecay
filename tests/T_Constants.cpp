#include "AnyHdecay.hpp"
#include "catch.hpp"
#include <memory>

TEST_CASE("Passing and retrieving constants", "[constants]") {

    SECTION("default constants") {
        auto p = AnyHdecay::SMParameters{};

        auto test = AnyHdecay::Hdecay(p);

        auto cp = test.smParameters();

        CHECK(p.alphaS == cp.alphaS);
        CHECK(p.mS == cp.mS);
        CHECK(p.mC == cp.mC);
        CHECK(p.mB == cp.mB);
        CHECK(p.mT == cp.mT);
        CHECK(p.mTau == cp.mTau);
        CHECK(p.mMu == cp.mMu);
        CHECK(p.invAlpha == cp.invAlpha);
        CHECK(p.GF == cp.GF);
        CHECK(p.widthW == cp.widthW);
        CHECK(p.widthZ == cp.widthZ);
        CHECK(p.mZ == cp.mZ);
        CHECK(p.mW == cp.mW);
        CHECK(p.ckmTB == cp.ckmTB);
        CHECK(p.ckmTS == cp.ckmTS);
        CHECK(p.ckmTD == cp.ckmTD);
        CHECK(p.ckmCB == cp.ckmCB);
        CHECK(p.ckmCS == cp.ckmCS);
        CHECK(p.ckmCD == cp.ckmCD);
        CHECK(p.ckmUB == cp.ckmUB);
        CHECK(p.ckmUS == cp.ckmUS);
        CHECK(p.ckmUD == cp.ckmUD);
    }

    SECTION("modified constants") {
        auto p = AnyHdecay::SMParameters{};

        p.mB = 5;
        p.mT = 150;
        p.invAlpha = 138;
        p.ckmTB = 0.;

        auto test = AnyHdecay::Hdecay(p);

        auto cp = test.smParameters();

        CHECK(p.alphaS == cp.alphaS);
        CHECK(p.mS == cp.mS);
        CHECK(p.mC == cp.mC);
        CHECK(p.mB == cp.mB);
        CHECK(p.mT == cp.mT);
        CHECK(p.mTau == cp.mTau);
        CHECK(p.mMu == cp.mMu);
        CHECK(p.invAlpha == cp.invAlpha);
        CHECK(p.GF == cp.GF);
        CHECK(p.widthW == cp.widthW);
        CHECK(p.widthZ == cp.widthZ);
        CHECK(p.mZ == cp.mZ);
        CHECK(p.mW == cp.mW);
        CHECK(p.ckmTB == cp.ckmTB);
        CHECK(p.ckmTS == cp.ckmTS);
        CHECK(p.ckmTD == cp.ckmTD);
        CHECK(p.ckmCB == cp.ckmCB);
        CHECK(p.ckmCS == cp.ckmCS);
        CHECK(p.ckmCD == cp.ckmCD);
        CHECK(p.ckmUB == cp.ckmUB);
        CHECK(p.ckmUS == cp.ckmUS);
        CHECK(p.ckmUD == cp.ckmUD);
    }
}

struct testconsts : public AnyHdecay::SMParameters {
    int stuff = 0;
    double morestuff = 1e20;
    const char *evenmorestuff = "stuff";
};

TEST_CASE("Custom constants class", "[constants]") {
    auto p = testconsts{};
    auto test = AnyHdecay::Hdecay(p);
    auto cp = test.smParameters();

    CHECK(p.alphaS == cp.alphaS);
    CHECK(p.mS == cp.mS);
    CHECK(p.mC == cp.mC);
    CHECK(p.mB == cp.mB);
    CHECK(p.mT == cp.mT);
    CHECK(p.mTau == cp.mTau);
    CHECK(p.mMu == cp.mMu);
    CHECK(p.invAlpha == cp.invAlpha);
    CHECK(p.GF == cp.GF);
    CHECK(p.widthW == cp.widthW);
    CHECK(p.widthZ == cp.widthZ);
    CHECK(p.mZ == cp.mZ);
    CHECK(p.mW == cp.mW);
    CHECK(p.ckmTB == cp.ckmTB);
    CHECK(p.ckmTS == cp.ckmTS);
    CHECK(p.ckmTD == cp.ckmTD);
    CHECK(p.ckmCB == cp.ckmCB);
    CHECK(p.ckmCS == cp.ckmCS);
    CHECK(p.ckmCD == cp.ckmCD);
    CHECK(p.ckmUB == cp.ckmUB);
    CHECK(p.ckmUS == cp.ckmUS);
    CHECK(p.ckmUD == cp.ckmUD);
}
