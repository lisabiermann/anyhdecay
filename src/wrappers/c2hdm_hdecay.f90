subroutine hdecay_C2HDM(type2HDM, tbeta, re_m12sq, mHc, &
                        mH1, mH2, a1, a2, a3, &
                        smparams, &
                        BR) bind(C, name='hdecay_C2HDM')
    use, intrinsic :: iso_c_binding

    implicit none
    integer(kind=c_int), intent(in), value :: TYPE2HDM
    real(kind=c_double), intent(in), value :: tbeta, re_m12sq, mHc, &
                                              mH1, mH2, a1, a2, a3
    real(kind=c_double), intent(in) :: smparams(22)
    real(kind=c_double), intent(out) ::BR(66)

! common block variables from read_hdec
    double precision ams, amc, amb, amt
    double precision AMSB
    double precision amcb, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0, IHIGGS, NNLO
    integer IPOLE, IONSH, IONWZ, IOFSUSY, NFGG
    double precision TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM
    integer ITYPE2HDM, I2HDM, IPARAM2HDM
    integer IOELW
    double precision AMTP, AMBP, AMNUP, AMEP
    integer ISM4, IGGELW
    integer IFERMPHOB
    integer IMODEL
    integer INDIDEC
    double precision CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA
    integer ICOUPELW
    double precision AXMPL, AXMGD
    integer IGOLD
    double precision AMH1_C2HDM, AMH2_C2HDM, AMCH_C2HDM, &
        ALPH1_C2HDM, ALPH2_C2HDM, ALPH3_C2HDM, TGBET_C2HDM, AM12SQ_C2HDM
    integer IC2HDM, ITYPE_C2HDM
    double precision AMSM, AMA, AML, AMH, AMCH, AMAR

    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/THDM_HDEC/TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM, ITYPE2HDM, I2HDM, IPARAM2HDM
    COMMON/OMIT_ELW_HDEC/IOELW
    COMMON/SM4_HDEC/AMTP, AMBP, AMNUP, AMEP, ISM4, IGGELW
    COMMON/FERMIOPHOBIC_HDEC/IFERMPHOB
    COMMON/MODEL_HDEC/IMODEL
    COMMON/FLAGS_HDEC/INDIDEC
    COMMON/CPSM_HDEC/CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA, ICOUPELW
    COMMON/GOLDST_HDEC/AXMPL, AXMGD, IGOLD
    COMMON/CTHDM_HDEC_PARAM/AMH1_C2HDM, AMH2_C2HDM, AMCH_C2HDM, &
        ALPH1_C2HDM, ALPH2_C2HDM, ALPH3_C2HDM, TGBET_C2HDM, AM12SQ_C2HDM, &
        IC2HDM, ITYPE_C2HDM
    COMMON/HMASS_HDEC/AMSM, AMA, AML, AMH, AMCH, AMAR

! local variables
    double precision tgbet, alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber

! common block variables from WRITE_HDEC
    double precision hibrt(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), &
        hibrc(3), hibrg(3), hibrga(3), hibrzga(3), hibrw(3), hibrz(3), &
        hibrhpwm(3), hibrchch(3), hibrhihjz(3, 3), hibrhihjhk(3, 3, 3), &
        gamtot(3)
    double precision hcpbrl, hcpbrm, hcpbrs, hcpbrc, hcpbrb, hcpbrt, &
        hcpbrbu, hcpbrcd, hcpbrts, hcpbrtd, hcpbrwh(3), gamtotch
    COMMON/C2HDMHi_WIDTHS/hibrt, hibrb, hibrl, hibrm, &
        hibrs, hibrc, hibrg, hibrga, hibrzga, hibrw, &
        hibrz, hibrhpwm, hibrchch, hibrhihjz, hibrhihjhk, gamtot
    COMMON/C2HDMHc_WIDTHS/hcpbrl, hcpbrm, hcpbrs, hcpbrc, hcpbrb, hcpbrt, &
        hcpbrbu, hcpbrcd, hcpbrts, hcpbrtd, hcpbrwh, gamtotch

! common block variables for top decay
    double precision GAT, GAB, GLT, GLB, GHT, GHB, GZAH, GZAL, &
        GHHH, GLLL, GHLL, GLHH, GHAA, GLAA, GLVV, GHVV, &
        GLPM, GHPM, B, A
    COMMON/COUP_HDEC/GAT, GAB, GLT, GLB, GHT, GHB, GZAH, GZAL, &
        GHHH, GLLL, GHLL, GLHH, GHAA, GLAA, GLVV, GHVV, &
        GLPM, GHPM, B, A

    IHIGGS = 5
    IOELW = 0
    ISM4 = 0
    IGGELW = 0
    IFERMPHOB = 0
    I2HDM = 1
    IC2HDM = 1
    IMODEL = 0
    TGBET = tbeta

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    AMH1_C2HDM = mH1
    AMH2_C2HDM = mH2
    AMCH_C2HDM = mhc
    amhc2hdm = AMCH_C2HDM
    amch = AMCH_C2HDM
    ALPH1_C2HDM = a1
    ALPH2_C2HDM = a2
    ALPH3_C2HDM = a3
    TGBET_C2HDM = tbeta
    AM12SQ_C2HDM = re_m12sq
    ITYPE_C2HDM = type2HDM

    NNLO = 1
    IONSH = 0
    IONWZ = 0
    IPOLE = 0
    IOFSUSY = 1
    NFGG = 5
    IGOLD = 0
    INDIDEC = 0
    ICOUPELW = 0
! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

! set the Hc couplings used in the top decay calculation
    if (itype_c2hdm .eq. 1) then
        gat = 1.D0/tgbet_c2hdm
        gab = -1.D0/tgbet_c2hdm
    elseif (itype_c2hdm .eq. 2) then
        gat = 1.D0/tgbet_c2hdm
        gab = tgbet_c2hdm
    elseif (itype_c2hdm .eq. 3) then
        gat = 1.D0/tgbet_c2hdm
        gab = -1.D0/tgbet_c2hdm
    elseif (itype_c2hdm .eq. 4) then
        gat = 1.D0/tgbet_c2hdm
        gab = tgbet_c2hdm
    endif

    CALL HDEC_C2HDM(tgbet)

    BR = (/gamtot(1), hibrb(1), hibrl(1), hibrm(1), hibrs(1), hibrc(1), &
           hibrt(1), hibrg(1), hibrga(1), hibrzga(1), hibrw(1), hibrz(1), &
           hibrhpwm(1), hibrchch(1), &
           gamtot(2), hibrb(2), hibrl(2), hibrm(2), hibrs(2), hibrc(2), &
           hibrt(2), hibrg(2), hibrga(2), hibrzga(2), hibrw(2), hibrz(2), &
           hibrhihjz(2, 1), hibrhpwm(2), hibrhihjhk(2, 1, 1), hibrchch(2), &
           gamtot(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), hibrc(3), &
           hibrt(3), hibrg(3), hibrga(3), hibrzga(3), hibrw(3), hibrz(3), &
           hibrhihjz(3, 1), hibrhihjz(3, 2), hibrhpwm(3), &
           hibrhihjhk(3, 1, 1), hibrhihjhk(3, 2, 2), hibrhihjhk(3, 1, 2), &
           hibrchch(3), &
           gamtotch, hcpbrb, hcpbrl, hcpbrm, hcpbrs, hcpbrc, hcpbrt, &
           hcpbrcd, hcpbrbu, hcpbrts, hcpbrtd, &
           hcpbrwh(1), hcpbrwh(2), hcpbrwh(3), &
           gamt1, gamt0/gamt1, (gamt1 - gamt0)/gamt1/)

    return
end
